import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="base-parser",
    version="0.0.2",
    author="Barad & Riftin & Ori",
    description="A base parser class for implementing parsers using MQ Pub/Sub.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/riftool/base-parser",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
