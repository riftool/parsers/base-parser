import os
from abc import abstractmethod, ABCMeta

from queue_manager import QueueConsumer

from base_parser.logger import logger


class BaseParser(metaclass=ABCMeta):

    def __init__(self, exchange_name: str, product_type: str):
        self.exchange_name = exchange_name
        self.product_type = product_type
        self.logger = logger

    def _callback(self, channel, method, properties, body) -> None:
        self.logger.info('Consumed product.', extra={'properties': properties})
        self.send_product(self.parse_product(body))

    @abstractmethod
    def parse_product(self, product_buffer):
        """
        :return: The parsed product that be sent using the 'send_product' entry point.
        """
        raise NotImplementedError

    @abstractmethod
    def send_product(self, parsed_product) -> None:
        """
        An entry point for sending the product after the parsing method.
        :param parsed_product: the value returned from self.parse_product
        """
        pass

    def run(self):
        queue_headers = {'x-match': 'any', 'product_type': self.product_type}
        connection_string = os.environ.get('AMQP_URL')
        with QueueConsumer(connection_string, exchange_name=self.exchange_name, headers=queue_headers) as consumer:
            consumer.start_consuming(self._callback)
