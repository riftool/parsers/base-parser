# Riftool Base Parser

### OS environment variables
AMQP_URL for Rabbit MQ URL

### Use example
```python
import sys

from base_parser import BaseParser

class StdoutParser(BaseParser):
    
    def parse_product(self, product_buffer):
        return product_buffer.decode('utf8')
    
    def send_product(self, parsed_product) -> None:
        sys.stdout(parsed_product)

parser = StdoutParser(exchange_name='products', product_type='Files')
parser.run()
```